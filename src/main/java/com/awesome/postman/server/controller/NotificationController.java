package com.awesome.postman.server.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.awesome.postman.server.service.NotificationService;

@RestController	
@RequestMapping("/notification")
public class NotificationController {

	@Autowired
	private NotificationService service;

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/send/{to}", method = RequestMethod.POST)
	public void send(@PathVariable String to, @RequestBody String message)
			throws ClientProtocolException, IOException, URISyntaxException {
		this.service.send(to, message);
	}

}
