package com.awesome.postman.server.service;

public class NotificationWrapper {

	private String to;
	private Notification notification;
	
	public NotificationWrapper() {}
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public Notification getNotification() {
		return notification;
	}
	public void setNotification(Notification content) {
		this.notification = content;
	}
	
	
}
