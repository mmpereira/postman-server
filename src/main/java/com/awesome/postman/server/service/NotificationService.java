package com.awesome.postman.server.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class NotificationService {


	public void send(String to, String message) throws ClientProtocolException, IOException, URISyntaxException {
		
		
		
		final URI URL = new URI("https://gcm-http.googleapis.com/gcm/send");
		
		Header contentTypeHeader = new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		Header authorizationHeader = new BasicHeader(HttpHeaders.AUTHORIZATION, "key=AIzaSyBRz7f_KnZy48KzIgXR3j7iTQdwklJl98Q");
		
		
		List<Header> headers = new ArrayList<>();
		headers.add(contentTypeHeader);
		headers.add(authorizationHeader);
		
		HttpClient client = HttpClients.custom().setDefaultHeaders(headers).build();
		
		ObjectMapper mapper = new ObjectMapper();
		
		NotificationWrapper notification = createNotification(to, message);
		
		String json = mapper.writeValueAsString(notification);
		
		System.out.println(json);
		
		HttpEntity entity = new StringEntity(json);
		HttpUriRequest request = RequestBuilder.post().setUri(URL).setEntity(entity).build();
		
		HttpResponse response = client.execute(request);
		
		String result = EntityUtils.toString(response.getEntity());
		
		System.out.println(result);
	}

	private NotificationWrapper createNotification(String to, String message) {
		NotificationWrapper wrapper = new NotificationWrapper();
		wrapper.setTo(to);
		Notification notification = new Notification();
		notification.setTitle("Test Notification");
		notification.setBody(message);
		wrapper.setNotification(notification);
		
		return wrapper;
	}

}
